# Importing OpenCV package
import cv2
import pytesseract


cap = cv2.VideoCapture(0)

# void CascadeClassifier::detectMultiScale(
#     const Mat& image, 
#     vector<Rect>& objects, 
#     double scaleFactor,
#     int minNeighbors, 
#     int flags, 
#     Size minSize,
#     Size maxSize )
while True:
    _, img = cap.read()
    cropped_image = img[0:10, 0:10]
    cropped_dni = img[0:10, 0:10]

    
    # Converting image to grayscale
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    # Loading the required haar-cascade xml classifier file
    haar_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
    
    # Applying the face detection method on the grayscale image
    faces_rect = haar_cascade.detectMultiScale(gray_img, 1.1, 9)
    
    # Iterating through rectangles of detected faces
    for (x, y, w, h) in faces_rect:
        cv2.rectangle(img, (x-round(3.1*w), y-round(1.5*h)), (x+round(1.3*w), y+round(1.4*h)), (0, 255, 0), 2)

        if ((y-round(1.5*h)>0) and  x-round(3.1*w)>0):
            cropped_image = img[y-round(1.5*h):y-round(1.5*h)+round(2.9*h), x-round(3.1*w):x-round(3.1*w)+round(4.4*w)]
            height, width, _ = cropped_image.shape     
            cropped_dni = cropped_image[round(height*0.8):height, 0:round(width*0.35)]
            text = pytesseract.image_to_string(cropped_dni)
            print(text)
    cv2.imshow('Detected faces', img)
    cv2.imshow('Detected retall', cropped_dni)
    if cv2.waitKey(10) & 0xFF == ord('q'):
        break
