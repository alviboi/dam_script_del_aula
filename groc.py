#!/usr/bin/env python
import PySimpleGUI as sg
import cv2
import numpy as np
from PIL import Image, ImageDraw
import imutils
import time

"""
Demo program that displays a webcam using OpenCV
"""


def main():

    sg.theme('Black')

    h1 = 17
    s1 = 78
    v1 = 86
    h2 = 70
    s2 = 272
    v2 = 144
    # define the window layout
    layout = [[sg.Text('Colors', size=(40, 1), justification='center', font='Helvetica 20')],
              [sg.Image(filename='', key='image')],
              [sg.Text(h1, size=(5, 1), justification='center', font='Helvetica 12', key='h1'),
              sg.Text(s1, size=(5, 1), justification='center', font='Helvetica 12', key='s1'),
              sg.Text(v1, size=(5, 1), justification='center', font='Helvetica 12', key='v1'),],
              [sg.Text(h2, size=(5, 1), justification='center', font='Helvetica 12', key='h2'),
              sg.Text(s2, size=(5, 1), justification='center', font='Helvetica 12', key='s2'),
              sg.Text(v2, size=(5, 1), justification='center', font='Helvetica 12', key='v2'),],
              [sg.Button('h+', size=(5, 1), font='Helvetica 14'),
               sg.Button('h-', size=(5, 1), font='Helvetica 14'),
               sg.Button('s+', size=(5, 1), font='Helvetica 14'),
               sg.Button('s-', size=(5, 1), font='Helvetica 14'),
               sg.Button('v+', size=(5, 1), font='Helvetica 14'),
               sg.Button('v-', size=(5, 1), font='Helvetica 14'), ],
               [sg.Button('2h+', size=(5, 1), font='Helvetica 14'),
               sg.Button('2h-', size=(5, 1), font='Helvetica 14'),
               sg.Button('2s+', size=(5, 1), font='Helvetica 14'),
               sg.Button('2s-', size=(5, 1), font='Helvetica 14'),
               sg.Button('2v+', size=(5, 1), font='Helvetica 14'),
               sg.Button('2v-', size=(5, 1), font='Helvetica 14'), ], 
               [sg.Button('avant', size=(5, 1), font='Helvetica 14'),]
               ]


    window = sg.Window('Provant', layout, location=(800, 400))


    cap = cv2.VideoCapture(1)


    while True:
        _, frame = cap.read()
        hsv_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)    
        
             
        # Groc
        low = np.array([h1, s1, v1])
        high = np.array([h2, s2, v2])
        mask = cv2.inRange(hsv_frame, low, high)
        groc = cv2.bitwise_and(frame, frame, mask=mask)

        event, values = window.read(timeout=20)


        gray_img = cv2.cvtColor(groc, cv2.COLOR_BGR2GRAY)
        ret, binary_img = cv2.threshold(gray_img, 100, 255, cv2.THRESH_BINARY)
        binary_img2 = cv2.Canny(groc, 300, 300)
        cv2.imshow('Grey image', binary_img2)

        
        img = groc


        #canny = cv2.Canny(gray_img, 130, 255, 1)
        lines = cv2.HoughLinesP(gray_img, 1, np.pi/180, 60, np.array([]), 1, 1)


        img5=np.zeros((480,640,3),np.uint8)
        if lines is not None:
            for line in lines:
                for x1, y1, x2, y2 in line:
                    #print (x1 +" "+ y1 +" "+ x2 +" "+ y2)
                    cv2.line(groc, (x1, y1), (x2, y2), (255, 255, 255), 3)
                    img_mod=cv2.line(img5,(x1, y1), (x2, y2),(0,0,255),10)
        
        gray = cv2.cvtColor(img_mod, cv2.COLOR_BGR2GRAY)

        (ret, thresh) = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        edge = cv2.Canny(thresh, 100, 200)
        (cnts, _) = cv2.findContours(edge.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)


        total = 0
        for c in cnts:
            epsilon = 0.001 * cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, epsilon, True)

            cv2.drawContours(img_mod, [approx], -1, (0, 255, 0), 4)
            total += 1       





        cv2.imshow("Line",img_mod)




        binary_img2 = cv2.Canny(groc, 300, 300)

        contours, hierarchy = cv2.findContours(binary_img2, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        for contour in contours:
            cv2.drawContours(frame, [contour], 0, (0, 0, 255), 5)
        
        

        #laplacian = cv2.Laplacian(groc,cv2.CV_64F)
        
        #cv2.imshow('laplacian',laplacian)

        cv2.imshow('aproximacio2',frame)




    


        if event == 'h+':
            h1 += 3

        elif event == 'h-':
            h1 -= 3

        if event == 's+':
            s1 += 3

        elif event == 's-':
            s1 -= 3

        if event == 'v+':
            v1 += 3

        elif event == 'v-':
            v1 -= 3

        if event == '2h+':
            h2 += 3

        elif event == '2h-':
            h2 -= 3

        if event == '2s+':
            s2 += 3

        elif event == '2s-':
            s2 -= 3

        if event == '2v+':
            v2 += 3

        elif event == '2v-':
            v2 -= 3
        window['h1'].update(h1)
        window['v1'].update(v1)
        window['s1'].update(s1)
        window['h2'].update(h2)
        window['v2'].update(v2)
        window['s2'].update(s2)
        cv2.imshow("Groc", groc)






        # Calculation of Laplacian
        



        
        if cv2.waitKey(10) & 0xFF == ord('q'):
            break
        
        


main()