# Import socket module
import socket
 
 
def Main():

    host = '192.168.3.153'
 
    # Define the port on which you want to connect
    port = 12345
 
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
 
    # connect to server on local computer
    s.connect((host,port))
 
    # message you send to server
    while True:   
 
        # message received from server
        
 
        # print the received message
        # here it would be a reverse of sent message
        
 
        # ask the client whether he wants to continue
        ans = input('\nIntrodueix orde :')
        # message sent to server
        
        if ans == 'quit':
            break
        else:
            s.send(ans.encode('ascii'))
        data = s.recv(1024)
        print('Rebut de server server :',str(data.decode('ascii')))
    # close the connection
    s.close()
 
if __name__ == '__main__':
    Main()