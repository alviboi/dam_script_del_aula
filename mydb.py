import mysql.connector
import datetime

class base_dades:

    def __init__(self):
        self.mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="root",
        database="cefire_valencia"
        )

    def show_db(self):
        print(self.mydb)
        mycursor = self.mydb.cursor()
        mycursor.execute("SHOW DATABASES")
        for x in mycursor:
            print(x)

    def update_assistent (self, dni, data, codi):
        mycursor = self.mydb.cursor()
        mycursor.execute("UPDATE ctrl_codi SET Assistix = 1 WHERE Codi = \""+ codi +"\" AND DNI = "+ dni +" AND Data = \""+ data.strftime('%Y-%m-%d %H:%M:%S')+"\"")
        self.mydb.commit()

    def agafa_cursos_actuals(self):
        mycursor = self.mydb.cursor()
        hui = datetime.datetime.now().strftime('%Y-%m-%d')
        mycursor.execute("SELECT DISTINCT Codi, Data FROM ctrl_codi WHERE Data >= " + hui)
        myresult = mycursor.fetchall()
        return myresult
        #self.mydb.commit()

    def agafa_tots_els_assistents(self, Codi, Data ):
        mycursor = self.mydb.cursor()
        try:
            mycursor.execute("SELECT DISTINCT ctrl_assistents.DNI, ctrl_assistents.Nom FROM ctrl_codi right join ctrl_assistents ON ctrl_codi.DNI = ctrl_assistents.DNI WHERE ctrl_codi.Codi = \""+Codi+"\" AND ctrl_codi.Data = \""+Data+"\"")
            myresult = mycursor.fetchall()
        except mysql.connector.ProgrammingError as err:
            print(err.errno)
            print(err.sqlstate)
            print(err.msg)
            return -1
        except mysql.connector.Error as err:
            print(err)
            return -1
            
        return myresult

    def agafa_cursos_actuals_nom(self):
        mycursor = self.mydb.cursor()
        hui = datetime.datetime.now().strftime('%Y-%m-%d')
        mycursor.execute("SELECT DISTINCT ctrl_titul.Codi, ctrl_codi.Data, ctrl_titul.titul FROM ctrl_codi left join ctrl_titul on ctrl_titul.codi = ctrl_codi.Codi WHERE Data >= " + hui)
        myresult = mycursor.fetchall()
        return myresult


  
  
    # select * from (SELECT DISTINCT ctrl_assistents.DNI, ctrl_assistents.Nom, ctrl_codi.Codi as CODI FROM ctrl_codi right join ctrl_assistents ON ctrl_codi.DNI = ctrl_assistents.DNI WHERE ctrl_codi.Codi = '20VA47IN001' AND ctrl_codi.Data = '2022-04-24 17:30:00') AS tot left join ctrl_titul on tot.CODI = ctrl_titul.Codi;