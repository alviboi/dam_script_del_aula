import cv2
import pytesseract
import PySimpleGUI as sg
import os
import io
from PIL import Image
import sys
#import mydb
 

scriptDir = os.path.dirname(__file__)
im = os.path.join(scriptDir,"animacio2.gif")
#db = mydb.base_dades()

sg.theme('Dark')

stop_threads = False
titul_curs = "20VAXXINXXXX - Exemple Exemple Exemple"
detalls = " 1.- Assegura't que el DNI està recte i es veu en tota la pantalla \n\n 2.- Quan es detecte el DNI es vora un recuadre verd\n\n 3.- Espereu a que llegixca el número \n\n 4.- Una vegada identificat es mostrarà el teu nom \n\n 4.- Si hi ha cap problema posa't en contacte amb l'assessor"

cap = cv2.VideoCapture(0)
_, img = cap.read()
def crea_gui():   

    el1 = [[sg.Image(key = "Webcam")]]

    layout = [[sg.Stretch(),sg.Text(titul_curs, font='Helvetica 40', pad=(40,40), key='titul_curs'),sg.Stretch()],
            [sg.Image(im,  key = "Prog_bar"),sg.Text(detalls, font='Helvetica 30', pad=(20,20))],
            [sg.Column(el1, element_justification='c'),], #,scrollable=True
            [sg.Stretch(),
            sg.Button('Cursos', size=(10, 2), font='Helvetica 14'),
            sg.Button('About', size=(10, 2), font='Helvetica 14'),
            sg.Button('Exit', size=(10, 2), font='Helvetica 14'), ],]


    window = sg.Window('Window Title', layout, size=(1024,768), no_titlebar=False, location=(0,0), keep_on_top=True).Finalize()
    #window = sg.Window('Demo Application - OpenCV Integration', layout).Finalize()
    # window.Maximize()
    return window

def crea_gui2():
    print (__name__)
    #llistat = db.agafa_cursos_actuals_nom()
    #layout = [[sg.Combo(llistat, size=(70, 10), key="seleccionat")],
    #          [sg.Stretch(),sg.Button("Selecciona", size=(10, 1), font='Helvetica 14', pad=(20,40)), ]]
    #return sg.Window('Selecciona curs', layout, size=(500,200), finalize=True, keep_on_top=True)



def selecciona_curs(codi):

    print(__name__)


def DNI1(faces_rect, img, cropped_image):
    for (x, y, w, h) in faces_rect:

        cv2.rectangle(img, (x-round(3.1*w), y-round(1.5*h)), (x+round(1.3*w), y+round(1.4*h)), (0, 255, 0), 2)

        if ((y-round(1.5*h)>0) and  x-round(3.1*w)>0):
            cropped_image = img[y-round(1.5*h):y-round(1.5*h)+round(2.9*h), x-round(3.1*w):x-round(3.1*w)+round(4.4*w)]
            height, width, _ = cropped_image.shape     
            cropped_dni = cropped_image[round(height*0.8):height, 0:round(width*0.35)]
            text = pytesseract.image_to_string(cropped_dni)
            Arregla_DNI(text)
            #cv2.imshow('retall', cropped_dni)

def DNI2(faces_rect, img, cropped_image):
    for (x, y, w, h) in faces_rect:

        cv2.rectangle(img, (x-round(0.33*w), y-round(0.9*h)), (x+round(3.7*w), y+round(1.6*h)), (0, 255, 0), 2)

        if ((y-round(0.9*h)>0) and  x-round(0.33*w)>0):
            cropped_image = img[y-round(0.9*h):y-round(0.9*h)+round(2.5*h), x-round(0.33*w):x-round(0.33*w)+round(4.1*w)]
            height, width, _ = cropped_image.shape     
            cropped_dni = cropped_image[round(height*0.8):height, 0:round(width*0.35)]
            text = pytesseract.image_to_string(cropped_dni)
            Arregla_DNI(text)
            #cv2.imshow('retall', cropped_dni)

def Arregla_DNI (dni_captat):
    if len(dni_captat) != 0:
        for t in  dni_captat.split():
            if t.isdigit():
                print("DNI Captat: " + t[0:8])



def main():
    
    window1, window2 = crea_gui(), None     
    
    while True:
        window, event, values = sg.read_all_windows(timeout=20)

        if event == 'Exit':
            sys.exit()
        elif event == 'Cursos':
            crea_gui2()
            #sg.popup('Cursos')
        elif event == 'Hola':
            #crea_gui2()
            sg.popup('Hola')
        elif event == 'Selecciona':
            prova = values["seleccionat"]
            window1['titul_curs'].Update(prova[2])
            sg.popup(prova[2])
        elif event == 'About':
            sg.PopupNoWait('Exemple', keep_on_top=True)
        elif event == sg.WIN_CLOSED or event == 'Exit':
            window.close()
            if window == window2:       # if closing win 2, mark as closed
                window2 = None
            elif window == window1:     # if closing win 1, exit program
                break


        _, img2 = cap.read()

        img = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
        
        cropped_image = img[0:10, 0:10]
        cropped_dni = img[0:10, 0:10]

        #Provant gifs animats
        window1["Prog_bar"].UpdateAnimation(im,time_between_frames=200)

        gray_img = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
            
        haar_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
        
        #faces_rect = haar_cascade.detectMultiScale(gray_img, 1.1, 9)
        faces_rect = haar_cascade.detectMultiScale(gray_img, scaleFactor=1.1, minNeighbors=9, minSize=(100, 100), maxSize=(135, 135), flags=cv2.CASCADE_SCALE_IMAGE)
        # Iterating through rectangles of detected faces
        if len(faces_rect) != 0:
            valor_x = faces_rect[0][0]
            #print (valor_x)
            if valor_x > 320 and valor_x < 530:
                DNI1(faces_rect, img, cropped_image)
            elif valor_x > 32 and valor_x < 250:
                DNI2(faces_rect, img, cropped_image)

        #DNI2(faces_rect, img, cropped_image)

        # cv2.imshow('Tot', img)
        img2 = Image.fromarray(img)
        


        bio = io.BytesIO()  # a binary memory resident stream
        img2.save(bio, format= 'PNG')  # save image as png to it
        imgbytes = bio.getvalue()  # this can be used by OpenCV hopefully
        window1['Webcam'].Update(data=imgbytes)
        

if __name__ == "__main__":
    main()


