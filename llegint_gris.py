import cv2

# Enable camera
cap = cv2.VideoCapture(2)
cap.set(3, 640)
cap.set(4, 420)

# Convert input image to grayscale
while True:
    success, input_image = cap.read()
    gray_img = cv2.cvtColor(input_image, cv2.COLOR_BGR2GRAY)
    
    cv2.imshow('Grey image', gray_img)
    if cv2.waitKey(10) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()