import cv2
import pytesseract
import PySimpleGUI as sg
import os
import io
from PIL import Image
import sys
import mydb
import time
import json

import socket
 
# import thread module
#from _thread import *
#import _thread
#import threading
import multiprocessing
from queue import Queue



db = mydb.base_dades()


#THREADS
manager = multiprocessing.Manager()
dictionary = manager.dict()
stop_threads = False

# SOCKETS
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = ""
port = 12345
s.bind((host, port))
print("Connectat al port: ", port)
s.listen(5)

# GUI
titul_curs = ""
detalls = " 1.- Assegura't que el DNI està recte i es veu en tota la pantalla \n\n 2.- Quan es detecte el DNI es vora un recuadre verd\n\n 3.- Espereu a que llegixca el número \n\n 4.- Una vegada identificat es mostrarà el teu nom \n\n 4.- Si hi ha cap problema posa't en contacte amb l'assessor"
scriptDir = os.path.dirname(__file__)
im = os.path.join(scriptDir,"animacio2.gif")
im_cefire = os.path.join(scriptDir,"cefire.png")
sg.theme('Dark')


"""Crea l'enton gràfic de la pantalla del DNI
"""

def crea_gui(img):
    el1 = [[sg.Image(img, key = "Webcam")]]
    # el2 = [
    # [sg.Push(),sg.Text("DNI", font='Helvetica 60', pad=(40,40), key='dni_captat'),sg.Push()],
    # [sg.Push(),sg.Text("Nom exemple", font='Helvetica 60', pad=(40,40), key='nom_dni_curs'),sg.Push()],
    # [sg.Push(),sg.Text("ASSISTENT INSCRIT", text_color="green", visible=False,font='Helvetica 60', pad=(40,40), key='verdet'),sg.Push()],
    # ]

    el2 = [
    [sg.Push(),sg.Text("DNI", font='Helvetica 60', pad=(40,40), key='dni_captat'),sg.Push()],
    [sg.Push(),sg.Text("Nom exemple", font='Helvetica 40', pad=(20,20), key='nom_dni_curs'),sg.Push()],
    [sg.Push(),sg.Text("ASSISTENT INSCRIT", text_color="green", visible=False,font='Helvetica 40', pad=(20,20), key='verdet'),sg.Push()],
    ]

    layout = [[sg.Stretch(),sg.Text("", font='Helvetica 40', pad=(40,40), key='titul_curs'),sg.Stretch()],
            [sg.Image(im,  key = "Prog_bar"),sg.Text(detalls, font='Helvetica 30', pad=(20,20))],
            [sg.Stretch(),
            #sg.Button('Cursos', size=(10, 2), font='Helvetica 14'),
            sg.Button('About', size=(10, 2), font='Helvetica 14'),
            sg.Button('Exit', size=(10, 2), font='Helvetica 14'), ],
            [sg.Column(el1, element_justification='c'),sg.Column(el2, element_justification='c'),], #,scrollable=True
            ]


    window = sg.Window('Curs', layout, size=(1920,1080), no_titlebar=False, location=(0,0), keep_on_top=False).Finalize() #Canviar a true
    window.Maximize()
    return window
    """Crea l'enton gràfic de la pantalla per a seleccionar curs
    """

def crea_gui2():
    print (__name__)
    llistat = db.agafa_cursos_actuals_nom()
    layout = [
              [sg.Combo(llistat, size=(70, 10), key="seleccionat")],
              [sg.Stretch(),sg.Button("Selecciona", size=(10, 1), font='Helvetica 14', pad=(20,40)), ]]
    w = sg.Window('Selecciona curs', layout, size=(500,200), finalize=True, keep_on_top=True)
    # while True:
    #    window, event, values = sg.read_all_windows(timeout=20)
    #    if event == sg.WIN_CLOSED:
    #            w.close()

    """Crea l'enton gràfic de la pantalla inicial
    """

def crea_gui_intro():
    layout = [ [sg.Stretch(),sg.Image(im_cefire,pad=(0,80)),sg.Stretch(),],
            [sg.Stretch(), sg.Text("Espereu a que s'active el curs", font='Helvetica 40', pad=(40,40)),sg.Stretch()],
              [sg.Stretch(),sg.Button("Selecciona Curs", size=(30, 3), font='Helvetica 14', pad=(20,40)), sg.Stretch(),],
              [sg.Stretch(),sg.Button("Tanca", size=(30, 3), font='Helvetica 14', pad=(20,10)), sg.Stretch(),]]
    global window_main
    window_main = sg.Window('Selecciona curs', layout, finalize=True, keep_on_top=False)
    window_main.Maximize()

    """Aqeusta funció agafa la imatge captada per la webcam i crea una recuadre de color verd al DNI, després retalla eixe recuadre i
    a partir d'ahí agafa per proporcions el lloc on està el DNI. Aquest mètode és per al DNI model 1.
    """

def DNI1(faces_rect, img, cropped_image):
    for (x, y, w, h) in faces_rect:

        cv2.rectangle(img, (x-round(3.1*w), y-round(1.5*h)), (x+round(1.3*w), y+round(1.4*h)), (0, 255, 0), 2)

        if ((y-round(1.5*h)>0) and  x-round(3.1*w)>0):
            cropped_image = img[y-round(1.5*h):y-round(1.5*h)+round(2.9*h), x-round(3.1*w):x-round(3.1*w)+round(4.4*w)]
            height, width, _ = cropped_image.shape     
            cropped_dni = cropped_image[round(height*0.8):height, 0:round(width*0.35)]
            text = pytesseract.image_to_string(cropped_dni)
            return Arregla_DNI(text)
            #cv2.imshow('retall', cropped_dni)

    """Aqeusta funció agafa la imatge captada per la webcam i crea una recuadre de color verd al DNI, després retalla eixe recuadre i
    a partir d'ahí agafa per proporcions el lloc on està el DNI. Aquest mètode és per al DNI model 2.
    """

def DNI2(faces_rect, img, cropped_image):
    for (x, y, w, h) in faces_rect:

        cv2.rectangle(img, (x-round(0.33*w), y-round(0.9*h)), (x+round(3.7*w), y+round(1.6*h)), (0, 255, 0), 2)

        if ((y-round(0.9*h)>0) and  x-round(0.33*w)>0):
            cropped_image = img[y-round(0.9*h):y-round(0.9*h)+round(2.5*h), x-round(0.33*w):x-round(0.33*w)+round(4.1*w)]
            height, width, _ = cropped_image.shape     
            cropped_dni = cropped_image[round(height*0.8):height, 0:round(width*0.35)]
            text = pytesseract.image_to_string(cropped_dni)
            return Arregla_DNI(text)

            #cv2.imshow('retall', cropped_dni)

    """Quan es detecta un DNI a vegades es detecte números extranys (la lletra no ens interesa), així quan es capte un número de 8 xifres i és detectat
    com a número el tornem
    """

def Arregla_DNI (dni_captat):
    if len(dni_captat) != 0:
        for t in  dni_captat.split():
            if t.isdigit():
                dni_captat = t[0:8]
                print("DNI Captat: " + t[0:8])
                for x in assistents:
                    if str(x[0]) == dni_captat:
                        return x
            elif t[0] == "X": #Per si es tracta d'un NIE TODO: Provar un NIE
                dni_captat = t[0:8]
                print("DNI Captat: " + t[0:8])
                for x in assistents:
                    if str(x[0]) == dni_captat:
                        return x
    return [0,""] 

    """Aquesta funció és un procés que gestiona les connexions rebudes per socket
    """

def threaded(c):
    print("Entrant al thread principal")

    while True:
        data_r = c.recv(1024)       

        json_object = json.loads(data_r.decode())
        print(json_object["command"])
        print(str(json_object))
        print(str(dictionary["Arrancat"]))
        print("Envie")
        if dictionary["Arrancat"] and not json_object["command"] == "Para":
            c.send(b"Ja hi ha un proces arrancat \n")

        if json_object["command"] == "Arranca" and not dictionary["Arrancat"]:
            try:
                global assistents
                print(str(json_object["sessio"]))
                assistents = db.agafa_tots_els_assistents(json_object["codigo"], json_object["sessio"])
            except IndexError:
                assistents = -1
            print(assistents)       
            if assistents is None or assistents == -1:
                c.send(b"No hi ha resultats per a eixe curs en eixa data\n")
            else:
                print(b"Agafant participants")
                dictionary['Arrancat'] = True
                c.send(b"Arrancat correctament\n")
                p2 = multiprocessing.Process(target=principal, args=[assistents, json_object["codigo"], json_object["titul"]])
                # p2.daemon = True
                p2.start()
            #principal(c)
        elif json_object["command"] == "Para" and dictionary["Arrancat"]:
            print("Entre en para")
            p2.terminate()
            dictionary['Arrancat'] = False
            c.send(b"Rebut correctament: Para\n" )    
            #c.send(b"Rebut correctament: Para\n" )
            c.close()    
        #else:
        #    c.send(b"Orde inesperada\n")
        if not data_r:
            print('Bye')
            break

    c.close()

    """Es pot donar el cas que necessitem arrancar des del propi ordinador la pantalla del curs
    """

def arranca_manual(codigo, sessio, titul):
    print("Entrant al thread manual")
    while dictionary['Acaba']:
        if not dictionary["Arrancat"]:
            try:
                global assistents
                assistents = db.agafa_tots_els_assistents(codigo, sessio)
            except IndexError:
                assistents = -1
            print(assistents)       
            if assistents is None or assistents == -1:
                # sg.popup("No hi ha resultats per a eixe curs en eixa data")
                print("No hi ha resultats per a eixe curs en eixa data\n")
            else:
                print("Agafant participants")
                dictionary['Arrancat'] = True
                p2 = multiprocessing.Process(target=principal, args=[assistents, codigo, titul])
                p2.start()

            #principal(c)

    """En aquesta funció gestionem la entrada dels DNIs i la seua escritura a la base de dades
    """

def principal(assistents_curs, codigo, titul):
    cap = cv2.VideoCapture(0)
    _, img = cap.read()
    thread_run = True
    window1 = crea_gui(img)
    tit = codigo + ": " + titul
    window1["titul_curs"].Update(tit)
    start = 0  
    while thread_run:
        _, img2 = cap.read()
        img = img2
        #img = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
        cropped_image = img2[0:10, 0:10]
        cropped_dni = img2[0:10, 0:10]

        window, event, values = sg.read_all_windows(timeout=20)

        # Gestió dels botons

        if event == 'About':
            sg.PopupNoWait('Programa creat per Alfredo Rafael Vicente Boix per al CEFIRE de València', keep_on_top=True)
        elif event == 'Exit':   
            #window_main.close()       
            thread_run = False
            dictionary['Acaba'] = False
            dictionary['continua_socket'] = False
            window.close()
            s.close()
            # Tanquem l'ordinador cuanda s'acaba la sessió
            # os.system("shutdown now -h")
            break
            #if window == window2:
            #    window2 = None
            #elif window == window1:
            #    break


        
        window1["Prog_bar"].UpdateAnimation(im,time_between_frames=200)

        gray_img = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY) #Imatge a gris
            
        haar_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml") #Agafem el Viola Jones Classifier ja definit en la biblioteca per a cares
        
        #faces_8uhj                                                                                    rect = haar_cascade.detectMultiScale(gray_img, 1.1, 9)
        faces_rect = haar_cascade.detectMultiScale(gray_img, scaleFactor=1.1, minNeighbors=9, minSize=(100, 100), maxSize=(135, 135), flags=cv2.CASCADE_SCALE_IMAGE)
        
        if len(faces_rect) != 0:
            valor_x = faces_rect[0][0]
            #print (valor_x)
            dni_captat = [0,""]

            # Analitzem els dos DNIs en funció de si s'ha captat una cara o no
            if valor_x > 320 and valor_x < 530:
                dni_captat = DNI1(faces_rect, img, cropped_image)
            elif valor_x > 32 and valor_x < 250:
                dni_captat = DNI2(faces_rect, img, cropped_image)

            print(dni_captat)
            
            # Si tenim un número de DNI, el mostrem i mostrem l'actialització del Nom
            if dni_captat is not None and dni_captat[0] > 0: 
                window1["dni_captat"].Update(str(dni_captat[0]))
                window1["nom_dni_curs"].Update(dni_captat[1])
                window1["verdet"].Update(visible=True)
                start = time.time()
        # print(str(start))
        # print(str(time.time()))
        if time.time() > start + 3:
            window1["dni_captat"].Update("")
            window1["nom_dni_curs"].Update("")
            window1["verdet"].Update(visible=False)

        img = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
        img3 = Image.fromarray(img)
        # La transformació següent es realitza per a poder tenir una imatge en png per a poder ser salvada i utilitzar-la com entrenament per IA, i per a mostrarla a la webcam
        bio = io.BytesIO()  # a binary memory resident stream
        img3.save(bio, format= 'PNG')  # save image as png to it
        imgbytes = bio.getvalue()  # this can be used by OpenCV
        window1['Webcam'].Update(data=imgbytes)
    window.Close()
    
    cap.release()
    exit()
    print("Tancant")
    return True        

    """Mètode que gestiona l'inici
    """

def intro(l):
    window = crea_gui_intro()
    dictionary['Acaba'] = True
    while dictionary['Acaba']:
        window, event, values = sg.read_all_windows(timeout=20)
        if event == "Selecciona Curs":
            crea_gui2()
        if event == "Tanca":
            l.acquire()
            dictionary['continua_socket'] = False
            l.release()
            s.close()
            exit()
        if event == "Selecciona":
#############################################################################################################3
            print(str(values))
            if (len(values["seleccionat"]) == 0):
                sg.Popup("No has seleccionat res", keep_on_top=True)
            else:
                titul_curs = str(values["seleccionat"][0]) + " " + str(values["seleccionat"][2])
                print(titul_curs)
                values['titul_curs'] = titul_curs
                print(window["seleccionat"].Get())
                data_i =  str(values["seleccionat"][1])
                # 2022-06-08 18:00:00.0
                print(str(values["seleccionat"][1]))
                window.close()
                arranca_manual(str(values["seleccionat"][0]), data_i, str(values["seleccionat"][2]))
                # p = multiprocessing.Process(target=arranca_manual, args=(str(values["seleccionat"][0]),str(values["seleccionat"][1]),))
                # p.start()
                print(str(event))
        if event == "Exit":
            os._exit()
            window.close()
        elif event == sg.WIN_CLOSED:
            window.close()
    exit()

    """Pareix ser un bug de pysimplegui, cal arrancar els gui des d'un subthread
    """

def intro_thread(l):
    p0 = multiprocessing.Process(target=intro, args=[l])
    p0.start()        

    """Funció main, arranquem el socket
    """

def main():
    print("socket escoltant")
    continua_socket = True
    l = manager.Lock()
    intro_thread(l)
    l.acquire()
    dictionary['continua_socket'] = True
    dictionary['Arrancat'] = False
    l.release()
    while dictionary['continua_socket']:
        print("Entrant al thread de socket")
        c, addr = s.accept()
        print('Connectat a:', addr[0], ':', addr[1])
        p = multiprocessing.Process(target=threaded, args=(c,))   
        #####start_new_thread(threaded, (c,))
        # p.daemon = True
        p.start()

    s.close()
         
if __name__ == "__main__":
    main()


