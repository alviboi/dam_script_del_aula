#!/usr/bin/env python
import PySimpleGUI as sg
import cv2
import numpy as np


def main():

    sg.theme('Black')
    h1 = 94
    s1 = 80
    v1 = 2
    h2 = 116
    s2 = 255
    v2 = 255
    # define the window layout
    layout = [[sg.Text('Colors', size=(40, 1), justification='center', font='Helvetica 20')],
              [sg.Image(filename='', key='image')],
              [sg.Text(h1, size=(5, 1), justification='center', font='Helvetica 12', key='h1'),
              sg.Text(s1, size=(5, 1), justification='center', font='Helvetica 12', key='s1'),
              sg.Text(v1, size=(5, 1), justification='center', font='Helvetica 12', key='v1'),],
              [sg.Text(h2, size=(5, 1), justification='center', font='Helvetica 12', key='h2'),
              sg.Text(s2, size=(5, 1), justification='center', font='Helvetica 12', key='s2'),
              sg.Text(v2, size=(5, 1), justification='center', font='Helvetica 12', key='v2'),],
              [sg.Button('h+', size=(5, 1), font='Helvetica 14'),
               sg.Button('h-', size=(5, 1), font='Helvetica 14'),
               sg.Button('s+', size=(5, 1), font='Helvetica 14'),
               sg.Button('s-', size=(5, 1), font='Helvetica 14'),
               sg.Button('v+', size=(5, 1), font='Helvetica 14'),
               sg.Button('v-', size=(5, 1), font='Helvetica 14'), ],
               [sg.Button('2h+', size=(5, 1), font='Helvetica 14'),
               sg.Button('2h-', size=(5, 1), font='Helvetica 14'),
               sg.Button('2s+', size=(5, 1), font='Helvetica 14'),
               sg.Button('2s-', size=(5, 1), font='Helvetica 14'),
               sg.Button('2v+', size=(5, 1), font='Helvetica 14'),
               sg.Button('2v-', size=(5, 1), font='Helvetica 14'), ]]


    window = sg.Window('Provant', layout, location=(800, 400))


    cap = cv2.VideoCapture(3)

    while True:
        _, frame = cap.read()
        hsv_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        
        
             
        # Groc
        low = np.array([h1, s1, v1])
        high = np.array([h2, s2, v2])
        mask = cv2.inRange(hsv_frame, low, high)
        groc = cv2.bitwise_and(frame, frame, mask=mask)
        event, values = window.read(timeout=20)

        if event == 'h+':
            h1 += 3

        elif event == 'h-':
            h1 -= 3

        if event == 's+':
            s1 += 3

        elif event == 's-':
            s1 -= 3

        if event == 'v+':
            v1 += 3

        elif event == 'v-':
            v1 -= 3

        if event == '2h+':
            h2 += 3

        elif event == '2h-':
            h2 -= 3

        if event == '2s+':
            s2 += 3

        elif event == '2s-':
            s2 -= 3

        if event == '2v+':
            v2 += 3

        elif event == '2v-':
            v2 -= 3
        window['h1'].update(h1)
        window['v1'].update(v1)
        window['s1'].update(s1)
        window['h2'].update(h2)
        window['v2'].update(v2)
        window['s2'].update(s2)
        cv2.imshow("Groc", groc)

        if cv2.waitKey(10) & 0xFF == ord('q'):
            break
        
        


main()