import cv2

# Enable camera
cap = cv2.VideoCapture(2)
cap.set(3, 640)
cap.set(4, 420)

# Convert input image to grayscale
while True:
    success, input_image = cap.read()
    gray_img = cv2.cvtColor(input_image, cv2.COLOR_BGR2GRAY)
    threshold_value = gray_img[320, 210]
    print(threshold_value)
    ret, binary_img = cv2.threshold(gray_img, 100, 255, cv2.THRESH_BINARY)
    binary_img2 = cv2.Canny(input_image, 300, 300)
    cv2.imshow('Grey image', binary_img2)


    ret, thresh = cv2.threshold(input_image, 127, 255, 0)
    image, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    if cv2.waitKey(10) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()